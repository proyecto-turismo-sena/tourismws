// Define port conection.
process.env.PORT = process.env.PORT || 3000;

// Deveploment enviroment
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// Database conection
let conexDb = 'mongodb';
if (process.env.NODE_ENV === 'dev') {
    conexDb = `${conexDb}://localhost:27017/tourism`;
} else {
    conexDb = process.env.MONGO_URI;
}
process.env.URLDB = conexDb;