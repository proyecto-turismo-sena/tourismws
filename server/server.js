const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

require('./config/config');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Habilitar carpeta 'public'
app.use(express.static(path.resolve(__dirname, '../public')));

app.use(require('../src/routes/index'));

mongoose.connect(process.env.URLDB, { useNewUrlParser: true, useCreateIndex: true }, (err, resp) => {
    if (err) throw err;

    console.log('Connect to database');
});

app.listen(process.env.PORT, function() {
    console.log(`Example app listening on port ${ process.env.PORT }!`);
});