const express = require('express');
const Category = require('../models/Category');

const app = express();

/**
 * Obtiene lista de categorias disponibles
 */
app.get('/category', (req, res) => {
    Category.find()
    .exec((err, categoryDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            categories: categoryDb
        });
    });
});

/**
 * Inserta una nueva categoria en la base de datos.
 */
app.post('/category', (req, res) => {
    let body = req.body;

    let category = new Category({
        name: body.name
    });

    category.save((err, categoryDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!categoryDb) {
            return res.status(400).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            category: categoryDb
        });
    });
});

module.exports = app;