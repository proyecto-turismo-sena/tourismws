const express = require('express');
const fileUpload = require('express-fileupload');
const _ = require('underscore');
const fs = require('fs');
const path = require('path');

const Place = require('../models/Place');
const Comment = require('../models/Comments');

const app = express();

app.use(fileUpload());

/**
 * Obtener los sitios turisticos registrados con su respectiva categoria.
 */
app.get('/places', (req, res) => {
    Place.find({ state: true })
    .populate('category', 'name')
    .exec((err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        // Se consultan las calificaciones promedio de los sitios, se retornan agrupados
        // por el campo "_id" del sitio, es decir por el campo "place" en comments
        Comment.aggregate([
            {
                "$group": {
                    _id: "$place",
                    "place_rating": { 
                        $avg: "$rating"
                    }
                }
            }
        ])
        .exec((err, commentDb) => {
            if (err) {
                console.log(err);
            }
    
            // Se crea un arreglo vacio para almacenar los sitios.
            let placesRating = [];

            for(let i = 0; i < placeDb.length; i++) {
                // Se obtiene el sitio de la posicion actual.
                let place = placeDb[i];

                // Se busca si el sitio actual tiene calificacion.
                let rating = [];
                if (commentDb) {
                    rating = commentDb.filter(comment => String(comment._id) == String(place._id));
                }
                //console.log('rating:', rating.length);

                // Se crea un nuevo objecto json con la informacion del sitio,
                // si el sitio tiene calificacion se coloca, sino se coloca valor 0 (cero)
                var objTest = {
                    "state": place.state,
                    "_id": place._id,
                    "name": place.name,
                    "address": place.address,
                    "website": place.website,
                    "email": place.email,
                    "phone": place.phone,
                    "category": place.category,
                    "category_id": place.category_id,
                    "latitude": place.latitude,
                    "longitude": place.longitude,
                    "mobile_phone": place.mobile_phone,
                    "rating": rating.length > 0 ? rating[0].place_rating : 0,
                };
                // Se agrega el objeto al arreglo creado anteriormente.
                placesRating.push(objTest);
                //console.log(placesRating);
            };

            // Se retornan los sitios en el arreglo creado.
            return res.json({
                result: true,
                places: placesRating
            });
        });
    });
});

/**
 * Obtener los sitios turisticos registrados filtrando por el nombre.
 */
app.get('/places/:name', (req, res) => {
    let name = req.params.name;
    let regex = new RegExp(name, 'i');

    Place.find({ name: regex, state: true })
    .populate('category', 'name')
    .exec((err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            places: placeDb
        });
    });
});

/**
 * Obtener los sitios turisticos registrados filtrando por la categoria.
 */
app.get('/places/category/:category', (req, res) => {
    let category = req.params.category;
    let regex = new RegExp(category, 'i');

    Place.find({ category_id: regex, state: true})
    .populate('category', 'name')
    .exec((err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            places: placeDb
        });
    });
});

/**
 * Inserta un nuevo sitio turistico en la base de datos.
 */
app.post('/place', (req, res) => {
    let body = req.body;
    
    let place = new Place({
        name: body.name,
        address: body.address,
        website: body.website,
        email: body.email,
        mobile_phone: body.mobile_phone,
        phone: body.phone,
        category: body.category,
        category_id: body.category,
        latitude: body.latitude,
        longitude: body.longitude,
    });

    // Verifica si en los datos hay campos de tipo "file"
    if (req.files) {
        // Extensiones permitidas
        let validExtensions = ['png', 'jpg', 'gif', 'jpeg'];

        // Imagen 1
        if (req.files.image1) {
            let img1 = req.files.image1;
            let nameImg1 = img1.name.split('.');
            let extFile1 = nameImg1[nameImg1.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile1) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la primer imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile1
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img1.mv(`public/${img1.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture1 = img1.name;
        }
        
        // Imagen 2
        if (req.files.image2) {
            let img2 = req.files.image2;
            let nameImg2 = img2.name.split('.');
            let extFile2 = nameImg2[nameImg2.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile2) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la segunda imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile2
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img2.mv(`public/${img2.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture2 = img2.name;
        }

        // Imagen 3
        if (req.files.image3) {
            let img3 = req.files.image3;
            let nameImg3 = img3.name.split('.');
            let extFile3 = nameImg3[nameImg3.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile3) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la tercera imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile3
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img3.mv(`public/${img3.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture3 = img3.name;
        }
    }

    place.save((err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!placeDb) {
            return res.status(400).json({
                result: false,
                err
            });
        }

        return res.status(200).json({
            result: true,
            place: placeDb
        });
    });
});

/**
 * Actualizar la informacion del sitio turistico en la base de datos.
 */
app.put('/place/:id', (req, res) => {
    let id = req.params.id;
    //return res.json({place: req.body});
    let place = _.pick(req.body, ['name', 'address', 'website', 'email', 'mobile_phone', 'phone', 'latitude', 'longitude']);

    // Verifica si en los datos hay campos de tipo "file"
    if (req.files) {
        // Extensiones permitidas
        let validExtensions = ['png', 'jpg', 'gif', 'jpeg'];

        // Imagen 1
        if (req.files.image1) {
            let img1 = req.files.image1;
            let nameImg1 = img1.name.split('.');
            let extFile1 = nameImg1[nameImg1.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile1) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la primer imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile1
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img1.mv(`public/${img1.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture1 = img1.name;
        }
        
        // Imagen 2
        if (req.files.image2) {
            let img2 = req.files.image2;
            let nameImg2 = img2.name.split('.');
            let extFile2 = nameImg2[nameImg2.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile2) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la segunda imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile2
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img2.mv(`public/${img2.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture2 = img2.name;
        }

        // Imagen 3
        if (req.files.image3) {
            let img3 = req.files.image3;
            let nameImg3 = img3.name.split('.');
            let extFile3 = nameImg3[nameImg3.length - 1];
            // Valida que la extension del archivo sea una extension valida
            if (validExtensions.indexOf(extFile3) < 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'La extension de la tercera imagen no es valida, extensiones permitidas son: ' + validExtensions.join(', '),
                        extension: extFile3
                    }
                });
            }
            // Mueve el archivo seleccionado al folder de destino.
            img3.mv(`public/${img3.name}`);
            // Asigna el nombre del archivo al campo del modelo.
            place.picture3 = img3.name;
        }
    }

    Place.findByIdAndUpdate(id, place, (err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        return res.json({
            result: true,
            place: placeDb
        });
    });
});

app.delete('/place/:id', (req, res) => {
    let id = req.params.id;

    Place.findByIdAndUpdate(id, { state: false }, (err, placeDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!placeDb) {
            return res.status(404).json({
                result: false,
                err: { message: "Sitio no encontrado" }
            });
        }

        return res.json({
            result: true,
            place: placeDb
        });
    });
});

module.exports = app;