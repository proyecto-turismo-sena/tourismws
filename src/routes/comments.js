const express = require('express');

const Comment = require('../models/Comments');

const app = express();

/**
 * Consulta los comentarios de un sitio
 */
app.get('/comment/:place', (req, res) => {
    let placeId = req.params.place;
    let regex = new RegExp(placeId, 'i');

    Comment.find({ place_id: regex })
    .exec((err, commentDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }
        
        if (commentDb === undefined || commentDb.length === 0) {
            return res.status(404).json({
                result: false,
                err: { message: 'Este sitio aun no tiene comentarios'}
            });
        }

        return res.status(200).json({
            result: true,
            comments: commentDb
        });
    });
});

/**
 * Consuilta el rating promedio del sitio.
 */
app.get('/comment/rating/:place', (req, res) => {
    let placeId = req.params.place;

    Comment.aggregate([
        { "$match": { "place_id": placeId}},
        {
            "$group": {
                _id: "$place_id",
                "place_rating": { 
                    $avg: "$rating"
                }
            }
        }
    ])
    .exec((err, commentDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        //console.log(comments, comments[0].comments);

        return res.json({
            result: true,
            comments: commentDb
        });
    });
});

/**
 * Inserta un nuevo comentario y la calificacion para un sitio.
 */
app.post('/comment', (req, res) => {
    let body = req.body;

    let comment = new Comment({
        comment: body.comment,
        rating: body.rating,
        place: body.place,
        place_id: body.place
    });

    comment.save((err, commentDb) => {
        if (err) {
            return res.status(500).json({
                result: false,
                err
            });
        }

        if (!commentDb) {
            return res.status(400).json({
                result: false,
                err: { message: 'No se encontro la observación'}
            });
        }

        return res.status(200).json({
            result: true,
            comment: commentDb
        });
    });
});

module.exports = app;