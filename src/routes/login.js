const express = require('express');
const User = require('../models/User');

const app = express();

/**
 * Valida el ingreso del usuario administrador.
 */
app.post('/login', (req, res) => {
    let body =  req.body;
    
    if (!body.password) {
        return res.status(400).json({
            auth: false,
            message: 'La contraseña es obligatoria'
        });
    }

    if (!body.email && !body.username) {
        return res.status(400).json({
            auth: false,
            err: {
                message: 'El email o nombre de usuario son obligatorios'
            }
        });
    }

    User.findOne({ email: body.email }, (err, userDb) => {
        
        if (body.password !== userDb.password) {
            return res.status(400).json({
                auth: false,
                err: {
                    message: 'Usuario o contraseña incorrectos'
                }
            });
        }

        res.json({
            auth: true,
            usuario: userDb
        });
    });
});

module.exports = app;