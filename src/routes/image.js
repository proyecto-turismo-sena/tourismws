const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();

app.get('/image/:img', (req, res) => {
    let image = req.params.img;

    let pathImage = path.resolve(__dirname, `../../public/${image}`);

    if (fs.existsSync(pathImage)) {
        res.sendFile(pathImage);
    } else {
        const notImage = path.resolve(__dirname, '../../public/image-not-found.png');
        res.sendFile(notImage);
    }
});

module.exports = app;