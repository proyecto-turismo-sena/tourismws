const express = require('express');

const app = express();

app.use(require('./categories'));
app.use(require('./comments'));
app.use(require('./image'));
app.use(require('./login'));
app.use(require('./places'));

module.exports = app;