var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlaceSchema = new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        trim: true
    },
    address: {
        type: String,
        required: [true, 'La dirección es obligatoria'],
        trim: true
    },
    website: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    mobile_phone: {
        type: String,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    picture1: { type: String },
    picture2: { type: String },
    picture3: { type: String },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    category_id: {
        type: String,
        required: [true, 'La categoria es obligatoria']
    },
    latitude: { type: String },
    longitude: { type: String },
    state: {
        type: Boolean,
        default: true
    },
});

var Place = mongoose.model('Place', PlaceSchema);
module.exports = Place;