var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
    comment: {
        type: String,
        trim: true,
        required: [true, 'La observacion es obligatoria']
    },
    rating: {
        type: Number,
        trim: true
    },
    place: {
        type: Schema.Types.ObjectId,
        ref: 'Place'
    },
    place_id: {
        type: String,
        trim: true,
        required: [true, 'El sitio es obligatorio']
    }
});

var Comment = mongoose.model('Comment', CommentSchema);
module.exports = Comment;