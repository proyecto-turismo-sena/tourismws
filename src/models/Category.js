var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    }
});

var Category = mongoose.model('Category', CategorySchema);
module.exports = Category;